cd "$PSScriptRoot"

# 包含UI界面函数
. "$PSScriptRoot\dialog.ps1"

# 设置在线更新程序名称
$installer = "c:\Qt\MaintenanceTool.exe"

# 导入保存在csv文件里镜像服务器地址列表，包括name和addr两列
$mirror = Import-CSV mirror.csv

# 显示一个列表框显示所有服务器的名称供选择
$choosed = chooseList "选择镜像服务器" "请选择合适的镜像服务器" $mirror.name;

# 如果点了取消则退出
if($choosed -lt 0) { return }

# 根据选中的编号得到服务器地址
$server = $mirror.addr[$choosed]

# 如果不是安装在默认目录，需要查找目录
if(!(Test-Path $installer)){
    $tmp = Get-FileName "c:\Qt" "MaintenanceTool | MaintenanceTool.exe"
    if([String]::IsNullOrEmpty($tmp)) { return }
    $installer = $tmp;
    if(!(Test-Path $installer)){
        Write-Host $installer "不存在"
	    pause
        return
    }
}

# 生成windows版安装包所在目录
$REPO = $server + "/online/qtsdkrepository/windows_x86/desktop"

# 生成在线安装工具的命令行参数，目前添加了需要的版本5.12.9、5.15.1和qtcreator等基本工具
$option = "--accept-obligations --accept-licenses --st "
$option += $REPO+"/licenses,"
$option += $REPO+"/qt5_51212,"
$option += $REPO+"/qt5_51212_src_doc_examples,"
$option += $REPO+"/qt5_5152,"
$option += $REPO+"/qt5_5152_src_doc_examples,"
$option += $REPO+"/qt5_5152_wasm,"
$option += $REPO+"/qt6_622,"
$option += $REPO+"/qt6_622_src_doc_examples,"
$option += $REPO+"/qt6_622_wasm,"
$option += $REPO+"/tools_cmake,"
$option += $REPO+"/tools_maintenance,"
$option += $REPO+"/tools_maintenance_early_access,"
$option += $REPO+"/tools_maintenance_update_reminder,"
$option += $REPO+"/tools_generic,"
$option += $REPO+"/tools_openssl_src,"
$option += $REPO+"/tools_openssl_x64,"
$option += $REPO+"/tools_qtcreator,"
$option += $REPO+"/tools_vcredist,"
$option += $REPO+"/tools_mingw,"
$option += $REPO+"/tools_ifw,"
$option += $REPO+"/tools_ninja,"
$option += $REPO+"/tools_qtdesignstudio,"
$option += $REPO+"/tools_telemetry"

# 启动在线安装工具
[System.Diagnostics.Process]::Start($installer, $option)